package nivell1Strings;
import java.util.Scanner;
//Alex Alchapar
public class Exercici2 {
	public static void main(String[] args) {
		Scanner reader = new Scanner(System.in);
		System.out.print("Escriu una frase: ");
		String frase = reader.nextLine();
		int a = frase.length();
		for (int i = 0; i < a / 2; i++) {
			System.out.println(frase.charAt(i));
			System.out.println(frase.charAt(a - 1 - i));
		}
		if (frase.length() % 2 != 0) {
			System.out.print(frase.charAt(a / 2));
		}
		reader.close();
	}
}