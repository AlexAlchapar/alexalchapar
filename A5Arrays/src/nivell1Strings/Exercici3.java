package nivell1Strings;
import java.util.Scanner;
//Alex Alchapar
public class Exercici3 {
	public static void main(String[] args) {
		Scanner reader = new Scanner(System.in);
		System.out.print("Escriu un nom: ");
		String nom1 = reader.nextLine();
		nom1 = nom1.trim();
		nom1 = nom1.toLowerCase();
		System.out.print("Escriu un nom: ");
		String nom2 = reader.nextLine();
		nom2 = nom2.trim();
		nom2 = nom2.toLowerCase();
		int resultat= (nom1.compareTo(nom2));
		if (resultat == 0) {
			System.out.print("Us dieu igual!!!");
		} else {
			System.out.print("No us dieu igual.");
		}
		reader.close();
	}
}