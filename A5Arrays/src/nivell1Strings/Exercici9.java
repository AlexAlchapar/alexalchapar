package nivell1Strings;
import java.util.Scanner;
//Alex Alchapar
public class Exercici9 {
	public static void main(String[] args) {
		Scanner reader = new Scanner(System.in);
		System.out.print("Escriu una frase: ");
		String frase = reader.nextLine();
		frase = frase.toLowerCase();
		int j = frase.length() - 1, aux = 0;
		for (int i = 0; i <= j; i++, j--) {
			if (frase.charAt(i) != frase.charAt(j)) {
				aux++;
			}
		}
		if (aux > 0) {
			System.out.print("No es un palindrom.");
		} else {
			System.out.print("Si es un palindrom.");
		}
		reader.close();
	}
}