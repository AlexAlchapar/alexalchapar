package nivell1Strings;
import java.util.Scanner;
//Alex Alchapar
public class Exercici4 {
	public static void main(String[] args) {
		Scanner reader = new Scanner(System.in);
		int cont = 0, pos = 0;
		System.out.print("Escriu una frase: ");
		String frase = reader.nextLine();
		frase = frase.toLowerCase();
		pos = frase.indexOf("la");
		while (pos != -1) {
			cont++;
			pos = frase.indexOf("la", pos + 2);
		}
		System.out.print("Hi ha " + cont + " la");
		reader.close();
	}
}