package nivell1Strings;
import java.util.Scanner;
//Alex Alchapar
public class Exercici7 {
	public static void main(String[] args) {
		Scanner reader = new Scanner(System.in);
		char[] vocales = { 'a', 'e', 'i', 'o', 'u' };
		char[] consonantes = { 'b', 'c', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'm', 'n', '�', 'p', 'q', 'r', 's', 't', 'v',
				'w', 'x', 'y', 'z' };
		String vocal = "", consonante = "";
		System.out.print("Escriu una paraula: ");
		String frase = reader.nextLine();
		for (int i = 0; i < frase.length(); i++) {
			for (int j = 0; j < vocales.length; j++) {
				if (frase.charAt(i) == vocales[j]) {
					vocal += frase.charAt(i);
				}
			}
			for (int j = 0; j < consonantes.length; j++) {
				if (frase.charAt(i) == consonantes[j]) {
					consonante += frase.charAt(i);
				}
			}
		}
		System.out.println(vocal);
		System.out.println(consonante);
		reader.close();
	}
}