package nivell1Strings;
import java.util.Scanner;
//Alex Alchapar
public class Exercici1 {
	public static void main(String[] args) {
		Scanner reader = new Scanner(System.in);
		char letras[] = { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's',
				't', 'u', 'v', 'w', 'x', 'y', 'z' };
		int lletres = 0;
		System.out.print("Posa una frase: ");
		String frase = reader.nextLine();
		frase = frase.trim();
		frase = frase.toLowerCase();
		for (int i = 0; i < frase.length(); i++) {
			char letra = frase.charAt(i);
			for (int j = 0; j < letras.length; j++) {
				if (letra == letras[j]) {
					lletres++;
				}
			}
		}
		System.out.print(lletres);
		reader.close();
	}
}