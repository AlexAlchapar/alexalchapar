package nivell1Strings;
import java.util.Scanner;
//Alex Alchapar
public class Exercici8 {
	public static void main(String[] args) {
		Scanner reader = new Scanner(System.in);
		char letra;
		System.out.print("Escriu una frase: ");
		String frase = reader.nextLine();
		for (int i = frase.length() - 1; i >= 0; i--) {
			letra = frase.charAt(i);
			System.out.print(letra);
		}
		reader.close();
	}
}