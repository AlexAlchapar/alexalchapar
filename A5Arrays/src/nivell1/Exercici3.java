package nivell1;
import java.util.Random;
import java.util.Scanner;
//Alex Alchapar
public class Exercici3 {
	public static void main(String[] args) {
		Scanner reader = new Scanner(System.in);
		Random ran = new Random();
		int grans = 0, iguals = 0, petits = 0, enter = 0, cont = 0;
		final int NUMEROS = 20;
		int [] enters;
		enters = new int[NUMEROS];
		for (int i = 0; i < NUMEROS; i++) {
			int aleatori = ran.nextInt(101);
			enters [i] = aleatori;
		}
		while (cont < 1) {
			System.out.print("Posa un numero: ");
			if (reader.hasNextInt()) {
				enter = reader.nextInt();
				for (int i = 0; i < NUMEROS; i++) {
					if (enter < enters[i]) {
						grans++;
					} else if (enter > enters[i]) {
						petits++;
					} else {
						iguals++;
					}
			}
			System.out.println("Hi ha " + grans + " numeros m�s grans.");
			System.out.println("Hi ha " + petits + " numeros m�s petits.");
			System.out.println("Hi ha " + iguals + " numeros iguals.");
			cont++;
			} else {
				System.out.println("Has posat un caracter que no era un enter.");
				reader.nextLine();
			}
		}
		reader.close();
	}
}