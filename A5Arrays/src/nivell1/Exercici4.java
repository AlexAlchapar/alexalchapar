package nivell1;
import java.util.Scanner;
//Alex Alchapar
public class Exercici4 {
	public static void main(String[] args) {
		Scanner reader = new Scanner(System.in);
		int enter = 0, cero = 0, uno = 0, dos = 0, tres = 0, cuatro = 0, cinco = 0, seis = 0, siete = 0, ocho = 0,
				nueve = 0, diez = 0;
		final int NUMEROS = 6;
		int[] enters;
		enters = new int[NUMEROS];
		for (int i = 0; i < NUMEROS;) {
			System.out.print("Posa una nota: ");
			if (reader.hasNextInt()) {
				
				enter = reader.nextInt();
				enters[i] = enter;
				switch (enters[i]) {
				case 0:
					cero++;
					i++;
					break;
				case 1:
					uno++;
					i++;
					break;
				case 2:
					dos++;
					i++;
					break;
				case 3:
					tres++;
					i++;
					break;
				case 4:
					cuatro++;
					i++;
					break;
				case 5:
					cinco++;
					i++;
					break;
				case 6:
					seis++;
					i++;
					break;
				case 7:
					siete++;
					i++;
					break;
				case 8:
					ocho++;
					i++;
					break;
				case 9:
					nueve++;
					i++;
					break;
				case 10:
					diez++;
					i++;
					break;
				}
			} else {
				System.out.println("Error, tens que posar una nota entre 0 i 10.");
				i= 99;
			}
			if (i == 6) {
				System.out.println("Hi ha " + cero + " ceros.");
				System.out.println("Hi ha " + uno + " uns.");
				System.out.println("Hi ha " + dos + " dosos.");
				System.out.println("Hi ha " + tres + " tresos.");
				System.out.println("Hi ha " + cuatro + " quatres.");
				System.out.println("Hi ha " + cinco + " cincs.");
				System.out.println("Hi ha " + seis + " sisos.");
				System.out.println("Hi ha " + siete + " sets.");
				System.out.println("Hi ha " + ocho + " vuits.");
				System.out.println("Hi ha " + nueve + " nous.");
				System.out.println("Hi ha " + diez + " deus.");
			}
		}
		reader.close();
	}
}