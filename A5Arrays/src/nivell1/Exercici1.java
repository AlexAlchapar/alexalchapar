package nivell1;
import java.util.Scanner;
//Alex Alchapar
public class Exercici1 {
	public static void main(String[] args) {
		Scanner reader = new Scanner(System.in);
		int suma = 0, num = 0;
		final int NUM_ELEMENTS = 10;
		int [] enters;
		enters = new int[NUM_ELEMENTS];
		for (int i = 0; i < 10; i++) {
			System.out.print("Possa un numero enter: ");
			num = reader.nextInt();
			enters [i] = num;
			if (enters [i] % 3 == 0) {
				suma = suma + enters [i];
			}
		}
		System.out.print(suma);
		reader.close();
	}
}