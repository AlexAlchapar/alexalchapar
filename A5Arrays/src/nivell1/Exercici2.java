package nivell1;
import java.util.*;
//Alex Alchapar
public class Exercici2 {
	public static void main(String[] args) {
		Scanner reader = new Scanner(System.in);
		Random ran = new Random();
		final int NUMEROS = 5;
		int [] enters;
		enters = new int[NUMEROS];
		for (int i = 0; i < NUMEROS; i++) {
			int aleatori = ran.nextInt(11);
			enters [i] = aleatori;
		}
		System.out.print("Posa un numero: ");
		int num = reader.nextInt();
		for (int i = 0; i < NUMEROS; i++) {
			if (num == enters [i]) {
				System.out.print("Aquest numero si esta a la llista.");
				i = i + 10;
			}
			if (i == 4) {
				System.out.print("Aquest numero no esta a la llista.");
			}
		}
		reader.close();
	}
}