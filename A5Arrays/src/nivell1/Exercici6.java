package nivell1;
import java.util.Random;
import java.util.Scanner;
//Alex Alchapar
public class Exercici6 {
	public static void main(String[] args) {
		Scanner reader = new Scanner(System.in);
		Random ran = new Random();
		int opcion = 0, cont = 0;
		while (cont < 1) {
			do {
				System.out.println("1: Exercici 1");
				System.out.println("2: Exercici 2");
				System.out.println("3: Exercici 3");
				System.out.println("4: Exercici 4");
				System.out.println("5: Exercici 5");
				System.out.println("0: Acabar el programa");
				System.out.println("Escogeix una opcio: ");
				try {
					opcion = reader.nextInt();
				} catch (Exception e) {
					System.out.println("Tens que posar un numero enter");
					reader.nextLine();	
				}
			} while (opcion < 0 || opcion > 5);
			switch (opcion) {
			case 1:
				int suma = 0, num = 0;
				final int NUM_ELEMENTS = 10;
				int[] enters;
				enters = new int[NUM_ELEMENTS];
				for (int i = 0; i < 10; i++) {
					System.out.print("Possa un numero enter: ");
					num = reader.nextInt();
					enters[i] = num;
					if (enters[i] % 3 == 0) {
						suma = suma + enters[i];
					}
				}
				System.out.println(suma);
				break;
			case 2:
				final int NUMEROS = 5;
				int[] enters2;
				enters2 = new int[NUMEROS];
				for (int i = 0; i < NUMEROS; i++) {
					int aleatori = ran.nextInt(11);
					enters2[i] = aleatori;
				}
				System.out.print("Posa un numero: ");
				int num2 = reader.nextInt();
				for (int i = 0; i < NUMEROS; i++) {
					if (num2 == enters2[i]) {
						System.out.println("Aquest numero si esta a la llista.");
						i = i + 10;
					}
					if (i == 4) {
						System.out.println("Aquest numero no esta a la llista.");
					}
				}
				break;
			case 3:
				int grans = 0, iguals = 0, petits = 0, enter = 0, cont3 = 0;
				final int NUMEROS3 = 20;
				int[] enters3;
				enters3 = new int[NUMEROS3];
				for (int i = 0; i < NUMEROS3; i++) {
					int aleatori = ran.nextInt(101);
					enters3[i] = aleatori;
				}
				while (cont3 < 1) {
					System.out.print("Posa un numero: ");
					if (reader.hasNextInt()) {
						enter = reader.nextInt();
						for (int i = 0; i < NUMEROS3; i++) {
							if (enter < enters3[i]) {
								grans++;
							} else if (enter > enters3[i]) {
								petits++;
							} else {
								iguals++;
							}
						}
						System.out.println("Hi ha " + grans + " numeros m�s grans.");
						System.out.println("Hi ha " + petits + " numeros m�s petits.");
						System.out.println("Hi ha " + iguals + " numeros iguals.");
						cont3++;
					} else {
						System.out.println("Has posat un caracter que no era un enter.");
						reader.nextLine();
					}
				}
				break;
			case 4:
				int enter4 = 0, cero = 0, uno = 0, dos = 0, tres = 0, cuatro = 0, cinco = 0, seis = 0, siete = 0,
						ocho = 0, nueve = 0, diez = 0;
				final int NUMEROS4 = 6;
				int[] enters4;
				enters4 = new int[NUMEROS4];
				for (int i = 0; i < NUMEROS4;) {
					System.out.print("Posa una nota: ");
					if (reader.hasNextInt()) {

						enter4 = reader.nextInt();
						enters4[i] = enter4;
						switch (enters4[i]) {
						case 0:
							cero++;
							i++;
							break;
						case 1:
							uno++;
							i++;
							break;
						case 2:
							dos++;
							i++;
							break;
						case 3:
							tres++;
							i++;
							break;
						case 4:
							cuatro++;
							i++;
							break;
						case 5:
							cinco++;
							i++;
							break;
						case 6:
							seis++;
							i++;
							break;
						case 7:
							siete++;
							i++;
							break;
						case 8:
							ocho++;
							i++;
							break;
						case 9:
							nueve++;
							i++;
							break;
						case 10:
							diez++;
							i++;
							break;
						}
					} else {
						System.out.println("Error, tens que posar una nota entre 0 i 10.");
						i = 99;
					}
					if (i == 6) {
						System.out.println("Hi ha " + cero + " ceros.");
						System.out.println("Hi ha " + uno + " uns.");
						System.out.println("Hi ha " + dos + " dosos.");
						System.out.println("Hi ha " + tres + " tresos.");
						System.out.println("Hi ha " + cuatro + " quatres.");
						System.out.println("Hi ha " + cinco + " cincs.");
						System.out.println("Hi ha " + seis + " sisos.");
						System.out.println("Hi ha " + siete + " sets.");
						System.out.println("Hi ha " + ocho + " vuits.");
						System.out.println("Hi ha " + nueve + " nous.");
						System.out.println("Hi ha " + diez + " deus.");
					}
				}
				break;
			case 5:
				int aux = 0;
				final int NUMEROS5 = 10;
				int[] enters5;
				enters5 = new int[NUMEROS5];
				for (int i = 0; i < NUMEROS5; i++) {
					int aleatori = ran.nextInt(101);
					enters5[i] = aleatori;
				}
				for (int i = 0; i < NUMEROS5 - 1; i++) {
					for (int j = (i + 1); j < NUMEROS5; j++) {
						if (enters5[i] >= enters5[j]) {
							aux = enters5[i];
							enters5[i] = enters5[j];
							enters5[j] = aux;
						}
					}
				}
				for (int i = 0; i < NUMEROS5; i++) {
					if (i == enters5.length - 1) {
						System.out.println(enters5[i]);
					} else {
						System.out.print(enters5[i] + ", ");
					}
				}
				break;
			case 0:
				cont++;
				break;
			}
		}
		reader.close();
	}
}