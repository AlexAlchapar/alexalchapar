package nivell1;
import java.util.Random;
//Alex Alchapar
public class Exercici5 {
	public static void main(String[] args) {
		Random ran = new Random();
		int aux = 0;
		final int NUMEROS = 10;
		int [] enters;
		enters = new int[NUMEROS];
		for (int i = 0; i < NUMEROS; i++) {
			int aleatori = ran.nextInt(101);
			enters [i] = aleatori;
		}
		for (int i = 0; i < NUMEROS - 1; i++) {
			for (int j = (i + 1); j < NUMEROS; j++) {
				if (enters [i] >= enters [j]) {
					aux = enters[i];
					enters [i] = enters [j];
					enters [j] = aux;
				}
			}
		}
		for (int i = 0; i < NUMEROS; i++) {
			if (i == enters.length - 1) {
				System.out.println(enters[i]);
			} else {
				System.out.print(enters[i] + ", ");
			}
		}
	}
}