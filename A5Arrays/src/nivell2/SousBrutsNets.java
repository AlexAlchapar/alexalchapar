package nivell2;
import java.util.Scanner;
//Alex Alchapar
public class SousBrutsNets {
	@SuppressWarnings("unused")
	public static void main(String[] args) {
		Scanner reader = new Scanner(System.in);
		int opcio = 0, numempleat = 0;
		double sou = 0.0, op = 0.0, robo = 0.0, percent = 0.0;
		double [] sous;
		sous = new double[10];
		double [] neto;
		neto = new double[10];
		do {
			System.out.println("Gesti� dels sous");
			System.out.println("1. Introduir sous");
			System.out.println("2. Calcular sous nets");
			System.out.println("3. Modificar sou");
			System.out.println("4. Pujar sous");
			System.out.println("0. Sortir del programa");
			System.out.print("Opci�: ");
			do {
				try {
					opcio = reader.nextInt();
				} catch (Exception e) {
					System.out.println("Atenci�! �nicament es permet insertar n�meros. ");
					System.out.println("1. Introduir sous");
					System.out.println("2. Calcular sous nets");
					System.out.println("3. Modificar sou");
					System.out.println("4. Pujar sous");
					System.out.println("0. Sortir del programa");
					reader.nextLine();
				}
			} while (opcio < 0 || opcio > 4);
			switch (opcio) {
			case 1:
				for (int i = 0; i < 10;) {
					System.out.print("Posa el sou de l'empleat: ");
					sou = reader.nextDouble();
					if (sou >= 500 && sou <= 5000) {
						sous [i] = sou;
						i++;
					} else {
						System.out.println("El sou ha d'estar entre 500 i 5000!");
					}
				}
				break;
			case 2:
				for (int i = 0; i < 10; i++) {
					if (sous [i] <= 700) {
						robo = 8 + 6;
						op = robo * sous [i] / 100;
						neto [i] = sous [i] - op; 
					} else if (sous [i] <= 1100) {
						robo = 11 + 6;
						op = robo * sous [i] / 100;
						neto [i] = sous [i] - op;
					} else if (sous [i] <= 1500) {
						robo = 13 + 6;
						op = robo * sous [i] / 100;
						neto [i] = sous [i] - op;
					} else if (sous [i] <= 2100) {
						robo = 17 + 6;
						op = robo * sous [i] / 100;
						neto [i] = sous [i] - op;
					} else if (sous [i] <= 3000) {
						robo = 20 + 6;
						op = robo * sous [i] / 100;
						neto [i] = sous [i] - op;
					} else {
						robo = 25 + 6;
						op = robo * sous [i] / 100;
						neto [i] = sous [i] - op;
					}
				}
				break;
			case 3:
				System.out.println("Posa un numero d'empleat: ");
				numempleat = reader.nextInt();
				System.out.println(neto [numempleat]);
				break;
			case 4:
				System.out.print("Posa el percentatge que vols: ");
				percent = reader.nextDouble();
				break;
			case 0:
				System.out.println("Ad�u!");
				break;
			}
		} while (opcio != 0);
		reader.close();
	}
}