package nivell2;
import java.lang.Math;
import java.util.Scanner;
//Alex Alchapar
public class Loteria {
	@SuppressWarnings("unused")
	public static void main(String[] args) {
		Scanner reader = new Scanner(System.in);
		int max = 49, min = 1, rand = 0, numjug = 0, cont = 0;
		int range = max - min + 1;
		int [] combisorteig;
		combisorteig = new int[6];
		int [] combijugador;
		combijugador= new int[6];
		System.out.print("Quants diners hi ha al pot: ");
		int diners = reader.nextInt();
		for (int i = 0; i < 6; i++) {
			for (int j = 0; j < 1; j++) {
				rand = (int)(Math.random() * range) + min;
			}
			combisorteig [i] = rand;
		}
		for (int i = 0; i < 6; i++) {
			System.out.print("Posa els numeros que creus que seran els guanyadors: ");
			numjug = reader.nextInt();
			combijugador [i] = numjug;
		}
		for (int i = 0; i < 6; i++) {
			if (combisorteig [i] == combijugador [i]) {
				cont++;
			}
		}
		System.out.println("Et coincideixen " + cont + " numeros.");
		System.out.print("Els numeros del sorteig eren: ");
		for (int i = 0; i < 6; i++) {
			if (i == 5) {
				System.out.println(combisorteig[i]);
			} else {
				System.out.print(combisorteig[i] + ", ");
			}
		}
		System.out.print("Els teus numeros eren: ");
		for (int i = 0; i < 6; i++) {
			if (i == 5) {
				System.out.println(combijugador[i]);
			} else {
				System.out.print(combijugador[i] + ", ");
			}
		}
		if (cont == 2) {
			System.out.print("Has guanyat 5�!");
		} else if (cont == 3)  {
			System.out.print("Has guanyat 10�!");
		} else if (cont == 4)  {
			System.out.print("Has guanyat un 4% del premi!");
		} else if (cont == 5)  {
			System.out.print("Has guanyat un 5% del premi!");
		} else if (cont == 6)  {
			System.out.print("Has guanyat el 100% del premi!!!");
		} else {
			System.out.print("No has guanyat res.");
		}
		reader.close();
	}
}