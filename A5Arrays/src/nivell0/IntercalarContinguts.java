package nivell0;
public class IntercalarContinguts {
	public static void main(String[] args) {
		int[] primer = { 1, 3, 5, 7, 9, 11, 13, 15 };
		int[] segon = { 2, 4, 6, 8, 10, 12, 14, 16 };
		int[] tercer = new int[primer.length + segon.length];
		int index = 0;
		for (int i = 0; i < primer.length; i++) {
			tercer[index] = primer[i];
			index = index + 2;
		}
		index = 1;
		for (int i = 0; i < segon.length; i++) {
			tercer[index] = segon[i];
			index = index + 2;
		}
		for (int i = 0; i < tercer.length; i++) {
			System.out.print(tercer[i]);
			System.out.print("  ");
		}
	}
}